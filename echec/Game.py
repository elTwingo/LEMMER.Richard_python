import sys
import os
sys.path.append("Pieces")
sys.path.append("Directions")

from gameTools import *
from plateBuilder import buildPlate

charList = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

class Game:
    
    # Initialize Game
    def __init__(self):
      self.currentPlayer = "W"

      buildedPlate = buildPlate()
      self.plate = buildedPlate[0]
      self.whitePieces = buildedPlate[1]
      self.blackPieces = buildedPlate[2]
      self.whiteKing = buildedPlate[3]
      self.blackKing = buildedPlate[4]

      self.selectedPiece = ''
      self.play()

    # Define if a piece is playable
    def isPieceSelectable(self, x, y):
      piece = self.plate[x][y]
      if piece == ' ':
        return False
      if piece.getColor() != self.currentPlayer:
        return False
      if len(piece.getCasePossible(self.plate)) == 0:
        return False
      return True

    # Select a playable piece
    def getPiece(self, char, number):
      pos = convertUserPositions(char,number)
      x = pos[0]
      y = pos[1]

      if self.isPieceSelectable(x, y):
        self.selectedPiece = self.plate[x][y]
      else:
        print("\nAucune piece jouable trouvé")
        print("Veuillez réitérer votre séléction \n")

    # Game engine
    def play(self):
      while True:
        self.selectedPiece = ''
        print("C'est au tour du joueur ", self.currentPlayer, "\n")
        showPlate(self.plate)
        move=[]

        if self.isCheck(self.plate):
          print("Le joueur ", self.currentPlayer, " est en echec \n")
          move = self.leaveCheckState()
        else:
          move = self.movePiece()
        
        oldPos = move[0]
        pos = move[1]
        self.plate[oldPos[0]][oldPos[1]] = ' '
        self.plate[pos[0]][pos[1]] = self.selectedPiece
        print("Le joueur ",self.currentPlayer," déplace ", self.selectedPiece.display(), "\n")
        self.nextPlayer()

    # Wait movement of a player 
    def movePiece(self):
      while self.selectedPiece == '':
        print("Selectionnez une pièce à partir des coordonnées \n")
        pieceX = getValidChar()
        pieceY = getValidNumber() 
        self.getPiece(pieceX, pieceY)
      moveX = ''
      moveY = ''
      while self.selectedPiece.getColor() == self.currentPlayer:
        print("Entrez les coordonnées pour déplacer la pièce ", self.selectedPiece.display(), "\n")
        moveX = getValidChar()
        moveY = getValidNumber()
        pos = convertUserPositions(moveX,moveY)
        if self.selectedPiece.moveValid(self.plate, pos):
          oldPos = convertUserPositions(pieceX,pieceY)
          return [oldPos, pos]
        else:
          print("Vous ne pouvez pas déplacer cette pièce sur cette position")
      
    # Change player turn
    def nextPlayer(self):
      if self.currentPlayer == "W":
        self.currentPlayer = "B"
      else:
        self.currentPlayer = "W"

    # Check if a player is in check state
    def isCheck(self, plate):
      piecesMovesPossible = []
      piecesList = []
      targetKing = ''
      if self.currentPlayer == 'W':
        targetKing = self.whiteKing
        piecesList = self.blackPieces
      else:
        targetKing = self.blackKing
        piecesList = self.whitePieces
      for piece in piecesList:
        for move in piece.getCasePossible(plate):
          piecesMovesPossible.append(move)

      return targetKing.getLocation(plate) in piecesMovesPossible          

    
    # Wait the player to leave the check state
    def leaveCheckState(self):
      move = []
      
      while True:
        copy_plate = duplicatePlate(self.plate)   
        move = self.movePiece()
        oldPos = move[0]
        pos = move[1]
        copy_plate[oldPos[0]][oldPos[1]] = ' '
        copy_plate[pos[0]][pos[1]] = self.selectedPiece
        if self.isCheck(copy_plate):
          print("Le joueur ", self.currentPlayer, " est toujours en echec. Merci de rejouer \n")
          showPlate(self.plate)
          self.selectedPiece = ''
        else:
          return move

Game = Game()



