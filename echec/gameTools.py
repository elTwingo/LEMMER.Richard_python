charList = ['A','B','C','D','E','F','G','H']

# Make sure the character enter by the user is valid
def getValidChar():
  char = input("Entrez une lettre:")
  try:
    charList.index(char.upper())
  except:
    print("Entrez une lettre comprise entre A et H")
    char = getValidChar()
  return char.upper()

# Make sure the number enter by the user is valid
def getValidNumber():
  number = input("Entrez un chiffre:")
  try:
    ['1', '2', '3', '4', '5', '6', '7', '8'].index(number)
  except:
    print("Entrez un chiffre comprise entre 1 et 8")
    number = getValidNumber()
  return number

def convertUserPositions(char, number):
    return [charList.index(char.upper()), int(number)-1]

def convertPlatePositions(x, y):
    return [charList[x], str(y+1)]

# Display the plate in a pretty way
def showPlate(plate):
      xAxis = ' |'
      for char in charList:
        xAxis += ' ' + char + ' |'
      print(xAxis)
      for x in range(8):
        line = str(x+1)
        for y in range(8):
          if plate[y][x] != ' ':
            line += '| ' + plate[y][x].display()
          else :
            line += '|   '
        print(line, '|')
      print()

# Make a fidel copy of a grind
def duplicatePlate(plate):
    copy_plate = []
    for i in range(8):
      copy_plate.append([])
      for j in range(8):
        copy_plate[i].append(plate[i][j])
    return copy_plate
