
class FauxChessMan :
    def getColor(self):
        pass

    def getEnemyColor(self):
        pass

    def getLocation(self, plate):
        pass

    def getId(self):
        pass

    def display(self):
        pass

    def moveValid(self, plate, case):
        pass

    def getCasePossible(self, plate):
        pass


class Interface:

    def __init__(self, chessman: FauxChessMan):
        self._chessman = chessman

    def getCasePossible(self, plate):
        pass


class CrossDirection(Interface):

    def getCasePossible(self, plate):
        move = []
        pos = self._chessman.getLocation(plate)
        for x in range(pos[0]+1, 8):
            if plate[x][pos[1]] == ' ':
                move.append([x, pos[1]])
            else:
                if plate[x][pos[1]].getColor() == self._chessman.getEnemyColor():
                    move.append([x, pos[1]])
                break

        for x in range(pos[0]-1, -1, -1):
            if plate[x][pos[1]] == ' ':
                move.append([x, pos[1]])
            else:
                if plate[x][pos[1]].getColor() == self._chessman.getEnemyColor():
                    move.append([x, pos[1]])
                break

        for y in range(pos[1]+1, 8):
            if plate[pos[0]][y] == ' ':
                move.append([pos[0], y])
            else:
                if plate[pos[0]][y].getColor() == self._chessman.getEnemyColor():
                    move.append([pos[0], y])
                break

        for y in range(pos[1]-1, -1, -1):
            if plate[pos[0]][y] == ' ':
                move.append([pos[0], y])
            else:
                if plate[pos[0]][y].getColor() == self._chessman.getEnemyColor():
                    move.append([pos[0], y])
                break

        return move


class DiagonalDirection(Interface):

    def getCasePossible(self, plate):
        move = []
        pos = self._chessman.getLocation(plate)
        hasObstable = [False, False, False, False]
        for i in range(1, 8):
            x = pos[0] + i
            y = pos[1] + i
            if not hasObstable[0] and Helper.isOk(plate, x, y, self._chessman):
                move.append([x, y])
            else :
                hasObstable[0] = True

            x = pos[0] + i
            y = pos[1] - i
            if not hasObstable[1] and Helper.isOk(plate, x, y, self._chessman):
                move.append([x, y])
            else :
                hasObstable[1] = True

            x = pos[0] - i
            y = pos[1] - i
            if not hasObstable[2] and Helper.isOk(plate, x, y, self._chessman):
                move.append([x, y])
            else:
                hasObstable[2] = True

            x = pos[0] - i
            y = pos[1] + i
            if not hasObstable[3] and Helper.isOk(plate, x, y, self._chessman):
                move.append([x, y])
            else:
                hasObstable[3] = True

        return move


class KingDirection(Interface):

    def getCasePossible(self, plate):
        move = []
        pos = self._chessman.getLocation(plate)
        possibilities = [
            [pos[0], pos[1] + 1],
            [pos[0], pos[1] - 1],
            [pos[0] + 1, pos[1] + 1],
            [pos[0] + 1, pos[1] - 1],
            [pos[0] + 1, pos[1]],
            [pos[0] - 1, pos[1] - 1],
            [pos[0] - 1, pos[1]],
            [pos[0] - 1, pos[1] + 1],
        ]
        for x, y in possibilities:
            if Helper.isOk(plate, x, y, self._chessman):
                move.append([x, y])

        return move

class KnightDirection(Interface):

    def getCasePossible(self, plate):
        move = []
        pos = self._chessman.getLocation(plate)
        possibilities = [
            [pos[0] + 1, pos[1] + 2],
            [pos[0] + 2, pos[1] + 1],
            [pos[0] + 2, pos[1] - 1],
            [pos[0] + 1, pos[1] - 2],
            [pos[0] - 1, pos[1] - 2],
            [pos[0] - 2, pos[1] - 1],
            [pos[0] - 2, pos[1] + 1],
            [pos[0] - 1, pos[1] + 2],
        ]
        for x, y in possibilities:
            if Helper.isOk(plate, x, y, self._chessman):
                move.append([x, y])

        return move


class PawnDirection(Interface):

    def getCasePossible(self, plate):
        move = []
        sens = 1
        if self._chessman.getColor() == "B":
            sens = -1

        pos = self._chessman.getLocation(plate)
  
        pos[1] += 1 * sens
        
        if plate[pos[0]][pos[1]] == ' ':
            move.append(pos.copy())

        pos[0] -= 1
        if plate[pos[0]][pos[1]] != ' ' and plate[pos[0]][pos[1]].getColor() == self._chessman.getEnemyColor():
            move.append(pos.copy())

        pos[0] += 2
        if pos[0] < 8 and pos[0]>= 0 and pos[1] < 8 and pos[1]>= 0 and plate[pos[0]][pos[1]] != ' ' and plate[pos[0]][pos[1]].getColor() == self._chessman.getEnemyColor():
            move.append(pos.copy())

        return move


class Helper:

    def isOk(plate, x, y, chessman):
        return (x >= 0 and x < 8 and y >= 0 and y < 8)\
               and \
               (plate[x][y] == ' ' or (plate[x][y] != ' ' and plate[x][y].getColor() == chessman.getEnemyColor()))

