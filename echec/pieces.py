from movement import *

class ChessMan:
    count = 0
    _name = ""
    _direction = []

    def __init__(self, color):
        if color != 'W':
            self.color = 'B'
            self._enemy = 'W'
        else:
            self.color = 'W'
            self._enemy = 'B'
        self.__id = self.color + self._name[0] + str(ChessMan.count)
        ChessMan.count += 1

    def getColor(self):
        return self.color

    def getEnemyColor(self):
        if self.color == 'B':
            return 'W'
        else:
            return 'B'

    def getLocation(self, plate):
        pos = []
        for x in range(8):
            for y in range(8):
                if plate[x][y] != ' ' and plate[x][y].getId() == self.__id:
                    pos = [x, y]
        return pos

    def getId(self):
        return self.__id

    def display(self):
        return self.color + self._name[0]


    def moveValid(self, plate, case):
        move = self.getCasePossible(plate)
        if case in move:
            return True
        return False

    def getCasePossible(self, plate):
        move = []
        if len(self.getLocation(plate)) == 0:
            return move
        for direction in self._direction:
            for case in direction.getCasePossible(plate):
                move.append(case)
        return move

class King(ChessMan):
    _name = "®ing"

    def __init__(self, color):
        super().__init__(color)
        self._direction = [KingDirection(self)]

class Bishop(ChessMan):
    _name = "Bishop"

    def __init__(self, color):
        super().__init__(color)
        self._direction = [DiagonalDirection(self)]

class Knight(ChessMan):
    _name = "Knight"

    def __init__(self, color):
        super().__init__(color)
        self._direction = [KnightDirection(self)]

class Pawn(ChessMan):
    _name = "Pawn"

    def __init__(self, color):
        super().__init__(color)
        self._direction = [PawnDirection(self)]

class Queen(ChessMan):
    _name = "Queen"

    def __init__(self, color):
        super().__init__(color)
        self._direction = [CrossDirection(self), DiagonalDirection(self)]

class Rook(ChessMan):
    _name = "Rook"

    def __init__(self, color):
        super().__init__(color)
        self._direction = [CrossDirection(self)]