# Initialize plate
from pieces import *

def buildPlate():
  plate = []

  whitePieces = [
    Rook("W"),
    Knight("W"),
    Bishop("W"),
    King("W"),
    Queen("W"),
    Bishop("W"),
    Knight("W"),
    Rook("W")
  ]

  for i in range(8):
    whitePieces.append(Pawn("W"))

  blackPieces = [
    Rook("B"),
    Knight("B"),
    Bishop("B"),
    Queen("B"),
    King("B"),
    Bishop("B"),
    Knight("B"),
    Rook("B")
  ]

  for i in range(8):
    blackPieces.append(Pawn("B"))

  for i in range(8):
    plate.append([])
    for j in range(8):
      plate[i].append(' ')

  for i in range(8):
    plate[i][0] = whitePieces[i]
    plate[i][1] = whitePieces[i+8]
    plate[i][6] = blackPieces[i+8]
    plate[i][7] = blackPieces[i]

  return [ plate, whitePieces, blackPieces, whitePieces[3], blackPieces[4]]